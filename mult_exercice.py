import random
import time
import os
import platform
import pickle

t=time.time()
score=0
max_exercice = 100
jj=0


def clear_screen():
    if (platform.system()=='Linux'):
        os.system('clear')
    else:
        os.system('cls')


for i in range(max_exercice):
     x = random.randrange(2,10)
     z = random.randrange(2,10)
     print("(",i,")",z,"x",x," = ")
     while True:
         try:
             y=int(input())
             break
         except ValueError:
             print("vous devez entrer un nombre")
             continue
     if (int(y)==x*z):
         print("bien")
         score+=1
     else:
         print("la bonne reponse est ", z*x)

     #if (jj%4==0):
        #time.sleep(2)
        #clear_screen()
     
     jj+=1
     
print("votre score est de : ", score, "/",max_exercice , " bonnes reponses")
elapsed_time = time.time()-t;
print("vous avez mis ",elapsed_time," seconds pour faire les ",max_exercice," exercices")

actual_score = [score, elapsed_time]

if os.path.isfile('score_mult'):
    with open('score_mult','rb') as score1:
        mon_depickler = pickle.Unpickler(score1)
        objet = mon_depickler.load()
    if (objet[0]/objet[1] < actual_score[0]/actual_score[1]):
        print('bravo vous avez fait mieux que votre ancien meilleur score')
        with open('score_mult','wb') as score2:
            mon_pickle=pickle.Pickler(score2)
            mon_pickle.dump(actual_score)
    else:
        print('reessaie encore, tu feras mieux la prochaine fois')
else:
    with open('score_mult','wb') as score2:
        my_pic=pickle.Pickler(score2)
        my_pic.dump(actual_score)
