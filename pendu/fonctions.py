import random
import os
import platform

nombre_essai = 13
def lire_les_mots():
    mots=open('list_mots.txt','r')
    mots_string = mots.read()
    mots_list = mots_string.split()
    mots.close()
    return mots_list

def choisir_mot():
    aa = lire_les_mots()
    len_aa = len(aa)
    indice = random.randrange(len_aa)
    mot_choisi = aa[indice]
    return mot_choisi

def clear_screen():
    if (platform.system()=='Linux'):
        os.system('clear')
    else:
        os.system('cls')

def affiche_ecran(mot):
    clear_screen()
    print(mot)


def existe_lettre(mot, lettre):
    indices = []
    if lettre in mot:
        indices = [i for i, car in enumerate(mot) if car==lettre]

    return indices

def compose_mot(mot1, lettre, indice):
    ll = list(mot1)
    for i in range(len(indice)):
        ll[indice[i]] = lettre

    return "".join(ll)


def main():
    mot_choisi = choisir_mot()
    mot_affich = len(mot_choisi)*"*"
    affiche_ecran(mot_affich)
    count=0
    mot_trouve=False
    lettre_collection = list()

    while (not mot_trouve) and (count < nombre_essai):
        affiche_ecran(mot_affich)
        lettre = input('{} tester une lettre: '.format('('+str(count)+')'))
        if lettre in lettre_collection:
            continue
        
        lettre_collection.append(lettre)
        count+=1

        if lettre in mot_choisi:
            indices = existe_lettre(mot_choisi, lettre)
            mot_affich = compose_mot(mot_affich, lettre, indices)
            affiche_ecran(mot_affich)
            if "*" not in mot_affich:
                print('Bravo, tu as trouve le mot')
                mot_trouve=True
                break
    if count >= nombre_essai:
        print("PERDU, PENDU")
