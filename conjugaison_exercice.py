import random
import time
import os
import platform
import pickle
import ast

def clear_screen():
    if (platform.system()=='Linux'):
        os.system('clear')
    else:
        os.system('cls')


conj = open('conjugaison.txt','r')
#print(conj)
var=conj.read()
diction = ast.literal_eval(var)
#print(type(diction))
#print(diction)
conj.close() 
#print(list(diction.keys()))


maxiter = 100
list_verbe = list(diction.keys())
clear_screen()

for i in range(maxiter):
    x = random.randrange(len(list_verbe))
    conjug = diction[list_verbe[x]]
    list_conj = list(conjug.keys())
    print(list_verbe[x])
    y = random.randrange(len(list_conj))
    vc = input("{} ".format(list_conj[y]))

    if (vc == conjug[list_conj[y]]):
        print("bravo!")
    else:
        print("faux")
        print("la bonne reponse etait: {}".format(conjug[list_conj[y]]))
    
    print()
